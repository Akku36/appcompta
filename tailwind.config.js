const colors = require('tailwindcss/colors');

module.exports = {
    purge: [
        './templates/**/*.html.twig',
        './src/Form/*.php'
    ],
    theme: {
        container: {
            center: true,
        },
        extend: {
            colors:{
                lightblue: colors.lightBlue,
                gray: colors.trueGray
            },
            fontFamily: {
                roboto: ['Roboto'],
                lora: ['Lora'],
                rubik: ['Rubik']
            }
        }
    },
    plugins: [
   require('@tailwindcss/forms'),
    ],
}