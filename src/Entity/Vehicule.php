<?php

namespace App\Entity;

use App\Repository\VehiculeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=VehiculeRepository::class)
 */
class Vehicule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=70)
     */
    private $marque;

    /**
     * @ORM\Column(type="string", length=20)
     * @Groups ("deplacement:read")
     */
    private $immatriculation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $carteGrise;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPrincipal;


    /**
     * @ORM\ManyToOne(targetEntity=Intervenant::class, inversedBy="vehicules")
     * @ORM\JoinColumn(referencedColumnName="matricule")
     */
    private $Intervenant;

    /**
     * @ORM\ManyToOne(targetEntity=TypeVehicule::class, inversedBy="vehicules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $TypeVehicule;

    /**
     * @ORM\ManyToOne(targetEntity=Puissance::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $puissance;

    /**
     * @ORM\OneToMany(targetEntity=Deplacement::class, mappedBy="Vehicule")
     */
    private $deplacements;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activer = true;



    public function __toString()
    {
       return $this->immatriculation;
    }


    public function __construct()
    {
        $this->deplacements = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImmatriculation(): ?string
    {
        return $this->immatriculation;
    }

    public function setImmatriculation(string $immatriculation): self
    {
        $this->immatriculation = $immatriculation;

        return $this;
    }

    public function getCarteGrise(): ?string
    {
        return $this->carteGrise;
    }

    public function setCarteGrise(?string $carteGrise): self
    {
        $this->carteGrise = $carteGrise;

        return $this;
    }

    public function getIsPrincipal(): ?bool
    {
        return $this->isPrincipal;
    }

    public function setIsPrincipal(bool $isPrincipal): self
    {
        $this->isPrincipal = $isPrincipal;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getIntervenant()
    {
        return $this->Intervenant;
    }

    /**
     * @param mixed $Intervenant
     */
    public function setIntervenant($Intervenant): void
    {
        $this->Intervenant = $Intervenant;
    }


    public function getTypeVehicule(): ?TypeVehicule
    {
        return $this->TypeVehicule;
    }

    public function setTypeVehicule(?TypeVehicule $TypeVehicule): self
    {
        $this->TypeVehicule = $TypeVehicule;

        return $this;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * @return Collection|Deplacement[]
     */
    public function getDeplacements(): Collection
    {
        return $this->deplacements;
    }

    public function addDeplacement(Deplacement $deplacement): self
    {
        if (!$this->deplacements->contains($deplacement)) {
            $this->deplacements[] = $deplacement;
            $deplacement->setVehicule($this);
        }

        return $this;
    }

    public function removeDeplacement(Deplacement $deplacement): self
    {
        if ($this->deplacements->removeElement($deplacement)) {
            // set the owning side to null (unless already changed)
            if ($deplacement->getVehicule() === $this) {
                $deplacement->setVehicule(null);
            }
        }

        return $this;
    }

    public function getPuissance(): ?Puissance
    {
        return $this->puissance;
    }

    public function setPuissance(?Puissance $puissance): self
    {
        $this->puissance = $puissance;

        return $this;
    }

    public function getActiver(): ?bool
    {
        return $this->activer;
    }

    public function setActiver(bool $activer): self
    {
        $this->activer = $activer;

        return $this;
    }
}
