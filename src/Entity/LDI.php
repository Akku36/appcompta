<?php

namespace App\Entity;

use App\Repository\LDIRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=LDIRepository::class)
 */
class LDI
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Intervenant::class, inversedBy="lDIs")
     * @ORM\JoinColumn(referencedColumnName="matricule")
     */
    private $matricule;


    /**
     * @ORM\Column(type="string", length=30)
     */
    private $code_analytique;


    /**
     * @ORM\Column(type="string", length=60)
     */
    private $codeStage;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $codeSession;

    /**
     * @ORM\Column(type="string", length=60)
     * @Groups ("deplacement:read")
     * @SerializedName("numéro")
     */
    private $ldiNumero;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $ldiNumeroPaiement;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $exerciceBudget;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $rubrique;

    /**
     * @ORM\Column(type="datetime", length=60)

     */

    private $dateDebutAnim;

    /**
     * @ORM\OneToMany(targetEntity=Deplacement::class, mappedBy="Ldi")
     */
    private $deplacements;

    public function __toString()
    {
        return $this->ldiNumero;
    }


    public function __construct()
    {
        $this->deplacements = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeStage(): ?string
    {
        return $this->codeStage;
    }

    public function setCodeStage(string $codeStage): self
    {
        $this->codeStage = $codeStage;

        return $this;
    }

    public function getCodeSession(): ?string
    {
        return $this->codeSession;
    }

    public function setCodeSession(string $codeSession): self
    {
        $this->codeSession = $codeSession;

        return $this;
    }

    public function getLdiNumero(): ?string
    {
        return $this->ldiNumero;
    }

    public function setLdiNumero(string $ldiNumero): self
    {
        $this->ldiNumero = $ldiNumero;

        return $this;
    }

    public function getLdiNumeroPaiement(): ?string
    {
        return $this->ldiNumeroPaiement;
    }

    public function setLdiNumeroPaiement(?string $ldiNumeroPaiement): self
    {
        $this->ldiNumeroPaiement = $ldiNumeroPaiement;

        return $this;
    }


    public function getExerciceBudget(): ?string
    {
        return $this->exerciceBudget;
    }

    public function setExerciceBudget(string $exerciceBudget): self
    {
        $this->exerciceBudget = $exerciceBudget;

        return $this;
    }

    public function getRubrique(): ?string
    {
        return $this->rubrique;
    }

    public function setRubrique(string $rubrique): self
    {
        $this->rubrique = $rubrique;

        return $this;
    }

    public function getFormattedDateDebutAnim(): string
    {
        return $this->getDateDebutAnim()->format('d-m-Y H:i');
    }


    public function getDateDebutAnim(): ?DateTimeInterface
    {
        return $this->dateDebutAnim;
    }

    public function setDateDebutAnim(\DateTimeInterface $dateDebutAnim): self
    {
        $this->dateDebutAnim = $dateDebutAnim;

        return $this;
    }



    public function getCodeAnalytique(): ?string
    {
        return $this->code_analytique;
    }

    public function setCodeAnalytique(string $code_analytique): self
    {
        $this->code_analytique = $code_analytique;

        return $this;
    }

    public function getMatricule(): ?Intervenant
    {
        return $this->matricule;
    }

    public function setMatricule(?Intervenant $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    /**
     * @return Collection|Deplacement[]
     */
    public function getDeplacements(): Collection
    {
        return $this->deplacements;
    }

    public function addDeplacement(Deplacement $deplacement): self
    {
        if (!$this->deplacements->contains($deplacement)) {
            $this->deplacements[] = $deplacement;
            $deplacement->setIdLdi($this);
        }

        return $this;
    }

    public function removeDeplacement(Deplacement $deplacement): self
    {
        if ($this->deplacements->removeElement($deplacement)) {
            // set the owning side to null (unless already changed)
            if ($deplacement->getIdLdi() === $this) {
                $deplacement->setIdLdi(null);
            }
        }

        return $this;
    }

}
