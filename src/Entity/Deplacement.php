<?php

namespace App\Entity;

use App\Repository\DeplacementRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=DeplacementRepository::class)
 */
class Deplacement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups ("deplacement:read")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups ("deplacement:read")
     */
    private $kms;

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @Groups ("deplacement:read")
     */
    private $montant;

    /**
     * @ORM\Column(type="date")
     * @Groups ("deplacement:read")
     */
    private $dateDeplacement;

    /**
     * @ORM\Column(type="integer")
     */
    private $exerciceBudget;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateValidation;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $validationCompta;


    /**
     * @ORM\ManyToOne(targetEntity=Intervenant::class, inversedBy="deplacements")
     * @ORM\JoinColumn(referencedColumnName="matricule")
     */
    private $Intervenant;

    /**
     * @ORM\ManyToOne(targetEntity=Vehicule::class, inversedBy="deplacements")
     * @ORM\JoinColumn(referencedColumnName="id")
     * @Groups ("deplacement:read")
     */
    private $Vehicule;

    /**
     * @ORM\ManyToOne(targetEntity=LDI::class, inversedBy="deplacements")
     * @ORM\JoinColumn(referencedColumnName="id")
     * @Groups ("deplacement:read")
     */
    private $Ldi;

    /**
     * @ORM\Column(type="string")
     */
    private $createur;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isRegul;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKms(): ?int
    {
        return $this->kms;
    }

    public function setKms(int $kms): self
    {
        $this->kms = $kms;

        return $this;
    }

    public function getMontant(): ?float
    {
        return $this->montant;
    }

    public function setMontant(float $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getDateDeplacement(): ?DateTimeInterface
    {
        return $this->dateDeplacement;
    }

    public function setDateDeplacement(DateTimeInterface $dateDeplacement): self
    {
        $this->dateDeplacement = $dateDeplacement;

        return $this;
    }

    public function getExerciceBudget(): ?int
    {
        return $this->exerciceBudget;
    }

    public function setExerciceBudget(int $exerciceBudget): self
    {
        $this->exerciceBudget = $exerciceBudget;

        return $this;
    }

    public function getDateValidation(): ?DateTimeInterface
    {
        return $this->dateValidation;
    }

    public function setDateValidation(?DateTimeInterface $dateValidation): self
    {
        $this->dateValidation = $dateValidation;

        return $this;
    }

    public function getValidationCompta(): ?bool
    {
        return $this->validationCompta;
    }

    public function setValidationCompta(bool $validationCompta): self
    {
        $this->validationCompta = $validationCompta;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getIntervenant()
    {
        return $this->Intervenant;
    }

    /**
     * @param mixed $Intervenant
     */
    public function setIntervenant($Intervenant): void
    {
        $this->Intervenant = $Intervenant;
    }

    /**
     * @return mixed
     */
    public function getVehicule()
    {
        return $this->Vehicule;
    }

    /**
     * @param mixed $Vehicule
     */
    public function setVehicule($Vehicule): void
    {
        $this->Vehicule = $Vehicule;
    }

    /**
     * @return mixed
     */
    public function getLdi()
    {
        return $this->Ldi;
    }

    /**
     * @param mixed $Ldi
     */
    public function setLdi($Ldi): void
    {
        $this->Ldi = $Ldi;
    }

    public function getCreateur()
    {
        return $this->createur;
    }

    public function setCreateur(string $createur): self
    {
        $this->createur = $createur;

        return $this;
    }

    public function getIsRegul(): ?bool
    {
        return $this->isRegul;
    }

    public function setIsRegul(bool $isRegul): self
    {
        $this->isRegul = $isRegul;

        return $this;
    }



}
