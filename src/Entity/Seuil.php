<?php

namespace App\Entity;

use App\Repository\SeuilRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SeuilRepository::class)
 */
class Seuil
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $seuilMin;

    /**
     * @ORM\Column(type="integer")
     */
    private $seuilMax;

    /**
     * @ORM\ManyToOne(targetEntity=Puissance::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $puissanceFiscale;

    /**
     * @ORM\ManyToOne(targetEntity=TypeVehicule::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeVehicule;

    /**
     * @ORM\Column(type="float")
     */
    private $prixKm;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSeuilMin(): ?int
    {
        return $this->seuilMin;
    }

    public function setSeuilMin(int $seuilMin): self
    {
        $this->seuilMin = $seuilMin;

        return $this;
    }

    public function getSeuilMax(): ?int
    {
        return $this->seuilMax;
    }

    public function setSeuilMax(int $seuilMax): self
    {
        $this->seuilMax = $seuilMax;

        return $this;
    }

    public function getPuissanceFiscale(): ?Puissance
    {
        return $this->puissanceFiscale;
    }

    public function setPuissanceFiscale(?Puissance $puissanceFiscale): self
    {
        $this->puissanceFiscale = $puissanceFiscale;

        return $this;
    }

    public function getTypeVehicule(): ?TypeVehicule
    {
        return $this->typeVehicule;
    }

    public function setTypeVehicule(TypeVehicule $typeVehicule): self
    {
        $this->typeVehicule = $typeVehicule;

        return $this;
    }

    public function getPrixKm(): ?float
    {
        return $this->prixKm;
    }

    public function setPrixKm(float $prixKm): self
    {
        $this->prixKm = $prixKm;

        return $this;
    }
}
