<?php

namespace App\Entity;

use App\Repository\IntervenantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=IntervenantRepository::class)
 */
class Intervenant
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=15)
     * @Groups ("deplacement:read")
     */
    private $matricule;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=50)
     *
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $complementAdresse;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $telDomicile;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $telPro;

    /**
     * @ORM\OneToMany(targetEntity=Vehicule::class, mappedBy="Intervenant")
     */
    private $vehicules;

    /**
     * @ORM\OneToMany(targetEntity=LDI::class, mappedBy="matricule")
     */
    private $lDIs;

    /**
     * @ORM\OneToMany(targetEntity=Deplacement::class, mappedBy="Intervenant")
     */
    private $deplacements;

    public function __construct()
    {
        $this->vehicules = new ArrayCollection();
        $this->lDIs = new ArrayCollection();
        $this->deplacements = new ArrayCollection();
    }


    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse1(): ?string
    {
        return $this->adresse1;
    }

    public function setAdresse1(?string $adresse1): self
    {
        $this->adresse1 = $adresse1;

        return $this;
    }

    public function getComplementAdresse(): ?string
    {
        return $this->complementAdresse;
    }

    public function setComplementAdresse(?string $complementAdresse): self
    {
        $this->complementAdresse = $complementAdresse;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelDomicile(): ?string
    {
        return $this->telDomicile;
    }

    public function setTelDomicile(string $telDomicile): self
    {
        $this->telDomicile = $telDomicile;

        return $this;
    }

    public function getTelPro(): ?string
    {
        return $this->telPro;
    }

    public function setTelPro(string $telPro): self
    {
        $this->telPro = $telPro;

        return $this;
    }

    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    /**
     * @return Collection|Vehicule[]
     */
    public function getVehicules(): Collection
    {
        return $this->vehicules;
    }

    public function addVehicule(Vehicule $vehicule): self
    {
        if (!$this->vehicules->contains($vehicule)) {
            $this->vehicules[] = $vehicule;
            $vehicule->setIntervenant($this);
        }

        return $this;
    }

    public function removeVehicule(Vehicule $vehicule): self
    {
        if ($this->vehicules->removeElement($vehicule)) {
            // set the owning side to null (unless already changed)
            if ($vehicule->getIntervenant() === $this) {
                $vehicule->setIntervenant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LDI[]
     */
    public function getLDIs(): Collection
    {
        return $this->lDIs;
    }

    public function addLDI(LDI $lDI): self
    {
        if (!$this->lDIs->contains($lDI)) {
            $this->lDIs[] = $lDI;
            $lDI->setMatricule($this);
        }

        return $this;
    }

    public function removeLDI(LDI $lDI): self
    {
        if ($this->lDIs->removeElement($lDI)) {
            // set the owning side to null (unless already changed)
            if ($lDI->getMatricule() === $this) {
                $lDI->setMatricule(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Deplacement[]
     */
    public function getDeplacements(): Collection
    {
        return $this->deplacements;
    }

    public function addDeplacement(Deplacement $deplacement): self
    {
        if (!$this->deplacements->contains($deplacement)) {
            $this->deplacements[] = $deplacement;
            $deplacement->setMatricule($this);
        }

        return $this;
    }

    public function removeDeplacement(Deplacement $deplacement): self
    {
        if ($this->deplacements->removeElement($deplacement)) {
            // set the owning side to null (unless already changed)
            if ($deplacement->getMatricule() === $this) {
                $deplacement->setMatricule(null);
            }
        }

        return $this;
    }

}
