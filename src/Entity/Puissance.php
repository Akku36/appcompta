<?php

namespace App\Entity;

use App\Repository\PuissanceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PuissanceRepository::class)
 */
class Puissance
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libellePuissance;

    public function __toString()
    {
        return $this->libellePuissance;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibellePuissance(): ?string
    {
        return $this->libellePuissance;
    }

    public function setLibellePuissance(string $libellePuissance): self
    {
        $this->libellePuissance = $libellePuissance;

        return $this;
    }
}
