<?php

namespace App\Form;

use App\Entity\Intervenant;
use App\Entity\Puissance;
use App\Entity\TypeVehicule;
use App\Entity\Vehicule;
use App\Repository\IntervenantRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class VehiculeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('marque', TextType::class)
            ->add('immatriculation', TextType::class)
            ->add('carteGrise', FileType::class, [
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                        'mimeTypes' => [
                            'application/pdf',
                            'image/jpeg',
                            'image/png'
                        ],
                        'mimeTypesMessage' => 'Merci de choisi un fichier de type : pdf,jpeg ou png',
                    ])
                ],
            ])
            ->add('isPrincipal', CheckboxType::class, [
                'required' => false,
                'label' => 'Véhicule principal',
                'attr' => [
                    'class' => 'h-5 w-5'
                ]
            ])
            ->add('Intervenant', EntityType::class, [
                'class' => Intervenant::class,
                'attr' => [
                    'class' => 'hidden'
                ],
                'label' => false,
                'choice_label' => 'matricule',
                'query_builder' => function (IntervenantRepository $intervenantRepository) use ($options) {
                    return $intervenantRepository->createQueryBuilder('i')
                        ->where('i.matricule = ' . $options['matricule']);
                },
            ])
            ->add('TypeVehicule', EntityType::class, [
                'class' => TypeVehicule::class
            ])
            ->add('Puissance', EntityType::class,[
                'class' => Puissance::class,
                'choice_label' => 'libellePuissance'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Vehicule::class,
        ]);
        $resolver->setRequired('matricule');
    }
}
