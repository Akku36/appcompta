<?php

namespace App\Form;

use App\Entity\Deplacement;
use App\Entity\Intervenant;
use App\Entity\LDI;
use App\Entity\Vehicule;
use App\Repository\IntervenantRepository;
use App\Repository\LDIRepository;
use App\Repository\VehiculeRepository;
use DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeplacementType extends AbstractType
{
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add('kms', IntegerType::class, [
                'label' => 'Nombres de kilomètres',
            ])
            ->add('isRegul', CheckboxType::class, [
                'label' => "Regulation",
                'required' => false,
                'attr' => [
                    'class' => 'h-5 w-5',
                ]
            ])
            ->add('montant', NumberType::class, [
                'required' =>false,
                'attr' => [
                    'class' => 'hidden'
                ]
            ])
            ->add('dateDeplacement', DateType::class, [
                'widget' => 'text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'flex justify-center items-center'
                ]
            ])
            ->add('exerciceBudget', TextType::class, [

            ])
            ->add('dateValidation', DateType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'hidden'
                ]
            ])
            ->add('validationCompta', CheckboxType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'h-5 w-5',
                ]
            ])

            ->add('Intervenant', EntityType::class, [
                'attr' => [
                    'class' => 'hidden'
                ],
                'class' => Intervenant::class,
                'label' => false,
                'choice_label' => 'matricule',
                'query_builder' => function ( IntervenantRepository $intervenantRepository ) use ( $options ) {
                    return $intervenantRepository->createQueryBuilder('i')
                        ->where('i.matricule = ' . $options['matricule']);
                },
            ])
            ->add('Vehicule', EntityType::class, [
                'class' => Vehicule::class,
                'label' => 'Véhicule utilisé',
                'attr'=>[
                    'required'=>'required'
                ],
                'choice_label' => 'immatriculation',
                'query_builder' => function ( VehiculeRepository $vehiculeRepository ) use ( $options ) {
                    return $vehiculeRepository->createQueryBuilder('v')
                        ->where('v.Intervenant = ' . $options['matricule'])
                        ->andWhere('v.activer = true')
                        ->addOrderBy('v.isPrincipal', 'DESC');
                },
            ])
            ->add('Ldi', EntityType::class, [
                'class' => LDI::class,
                'label' => 'LDI concernée',
                'attr'=>[
                    'required'=>'required'
                ],
                'choice_label' =>  function (LDI $ldi) {
                    return $ldi->getLdiNumero(). ' (' . $ldi->getFormattedDateDebutAnim() .')';
                    },
                'query_builder' => function ( LDIRepository $LDIRepository ) use ( $options ) {
                    return $LDIRepository->createQueryBuilder('l')
                        ->where('l.matricule = ' . $options['matricule'])
                        ->orderBy('l.dateDebutAnim', 'DESC');
                },
            ]);

    }

    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults([
            'data_class' => Deplacement::class,
        ]);
        $resolver->setRequired('matricule');
    }
}
