<?php

namespace App\Form;

use App\Entity\Intervenant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IntervenantType extends AbstractType
{
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder
            ->add('matricule', TextType::class, [
                'attr' => [
                    'readonly' => true,
                    'class' => 'bg-gray-200'
                ]
            ])
            ->add('titre', ChoiceType::class, [
                'choices' => [
                    'Monsieur' => 'MR',
                    'Madame' => 'MME'
                ]
            ])
            ->add('prenom', TextType::class)
            ->add('nom', TextType::class)
            ->add('adresse1', TextType::class, [
                'label' => 'Adresse'
            ])
            ->add('complementAdresse', TextType::class, [
                'label' => 'Complément d\'adresse',
                'required' => false
            ])
            ->add('ville', TextType::class)
            ->add('codePostal', IntegerType::class, [
                'label' => 'Code Postal'
            ])
            ->add('email', EmailType::class)
            ->add('telDomicile', TextType::class)
            ->add('telPro', TextType::class);
    }

    public function configureOptions( OptionsResolver $resolver )
    {
        $resolver->setDefaults([
            'data_class' => Intervenant::class,
        ]);
    }
}
