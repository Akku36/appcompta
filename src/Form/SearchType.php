<?php

namespace App\Form;

use App\Custom\Search;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
   final public function buildForm(FormBuilderInterface $builder, array $options): void
   {
        $builder
            ->add('string', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Nom de l\'intervenant',
                    'class' => 'w-full',
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => "Rechercher",
                'row_attr' => [
                    'class' => 'bg-red-400 hover:bg-red-300 rounded text-white p-2 pl-4 pr-4',
                ]
            ]);
    }


    final public function getBlockPrefix(): string
    {
        return '';
    }

    final public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Search::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }
}