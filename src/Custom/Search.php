<?php


namespace App\Custom;


use Symfony\Component\Validator\Constraints as Assert;

class Search
{
    /**
     * @var string
     * @Assert\NotBlank
     *
     */
    public $string = "";

}

