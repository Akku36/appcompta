<?php


namespace App\Custom;


use App\Entity\Seuil;
use App\Repository\SeuilRepository;

class PriceCalculator
{

    /**
     * Calcul le prix du deplacement
     * @param $kmDuDeplacement
     * @param $puissanceFiscale
     * @param $typeVehicule
     * @param $totalKmAnnuel
     * @param SeuilRepository $seuilRepository
     * @return float|int
     */
    public function calculator( $kmDuDeplacement, $puissanceFiscale, $typeVehicule, $totalKmAnnuel, SeuilRepository $seuilRepository )
    {

        $nouveauTotal = $kmDuDeplacement + $totalKmAnnuel;

        $seuilNouveauTotal = new Seuil();
        $seuilTotalKmAnnuel = new Seuil();

        $seuils = $seuilRepository->findAll(); //On récupère tous les seuils en bd
        for ($i = 0; $i < count($seuils); $i++) {
            //pour chaque seuil on cherche le seuil qui correspond au nouveau total
            if ($nouveauTotal >= $seuils[$i]->getSeuilMin() && $nouveauTotal < $seuils[$i]->getSeuilMax()
                && $puissanceFiscale === $seuils[$i]->getPuissanceFiscale()->getLibellePuissance() && $typeVehicule === $seuils[$i]->getTypeVehicule()->getLibelleTypeVehicule()) {
                $seuilNouveauTotal = $seuils[$i];
            }
            //Dans le cas ou le nouveau total dépasse le seuil précédent on récupère le seuil précédent pour faire la premiere partie du calcul
            //EXEMPLE : déplacement de 750 km , totalKm annuel actuel de 1500; Le nouveau trajet va dépasser le seuil des 2000
            //nous devons donc diviser le calcul pour calculer la premiere partie des 500 km (entre 1500 et 200)
            //Puis calculer au nouveau taux les kilometres restant (250)
            if ($totalKmAnnuel >= $seuils[$i]->getSeuilMin() && $totalKmAnnuel < $seuils[$i]->getSeuilMax()
                && $puissanceFiscale === $seuils[$i]->getPuissanceFiscale()->getLibellePuissance() && $typeVehicule === $seuils[$i]->getTypeVehicule()->getLibelleTypeVehicule()) {
                $seuilTotalKmAnnuel = $seuils[$i];
            }
        }
        //Si on ne change pas de seuil = easy
        if ($seuilNouveauTotal === $seuilTotalKmAnnuel) {
            $coutDeplacement = $seuilNouveauTotal->getPrixKm() * $kmDuDeplacement;
        } else {
            //Sinon on calcul le cout pour chaque tranche de kilometres
            $kmSeuilPrecedent = $seuilTotalKmAnnuel->getSeuilMax() - $totalKmAnnuel;
            $kmSeuilActuel = $nouveauTotal - $seuilNouveauTotal->getSeuilMin();
            $coutDeplacement = $kmSeuilPrecedent * $seuilTotalKmAnnuel->getPrixKm() + $kmSeuilActuel * $seuilNouveauTotal->getPrixKm();

        }
        return $coutDeplacement;
    }
}