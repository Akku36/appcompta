<?php

namespace App\Service;

use App\Entity\Intervenant;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploader
{
    private $targetDirectory;
    private $slugger;

    public function __construct( $targetDirectory, SluggerInterface $slugger )
    {
        $this->targetDirectory = $targetDirectory;
        $this->slugger = $slugger;
    }

    /**
     * @param UploadedFile $file
     * @param $matricule
     * @return string
     * Permet l'upload des documents dans un chemin comprenant le matricule de l'intervenant
     * Afin de grouper les documents par intervenant
     * exemple public/uploads/44253
     */
    public function upload( UploadedFile $file, $matricule )
    {

        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename . '-' . uniqid('', true) . '.' . $file->guessExtension();

        try {
            $file->move($this->getTargetDirectory() . '/' . $matricule, $fileName);
        } catch (FileException $e) {
            $e = "Un problème est survenu lors du téléchargement de votre document";
        }

        return $fileName;
    }


    /**
     * @param $file
     * @param $matricule
     * Suppression d'un fichier
     */
    public function removeFile( $file, $matricule )
    {

        $file_path = $this->getTargetDirectory() . '/' . $matricule . '/' . $file;
        if (file_exists($file_path)) {
            unlink($file_path);
        }
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}
