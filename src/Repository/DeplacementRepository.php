<?php

namespace App\Repository;

use App\Entity\Deplacement;
use App\Entity\Intervenant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Deplacement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Deplacement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Deplacement[]    findAll()
 * @method Deplacement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeplacementRepository extends ServiceEntityRepository
{
    public function __construct( ManagerRegistry $registry )
    {
        parent::__construct($registry, Deplacement::class);
    }

    /**
     * @param Intervenant $intervenant
     * Retourne un tableau comprenant le total de kilomètres par années
     * Affiche les valeurs dans la section cumul kilomètrique
     * @return int|mixed[]|string
     */
    public function findKilometersPerYear( Intervenant $intervenant )
    {

        return $this->createQueryBuilder('d')
            ->select('d.exerciceBudget, SUM(d.kms) as kilometers')
            ->where('d.Intervenant = :intervenant')
            ->setParameter('intervenant', $intervenant)
            ->groupBy('d.exerciceBudget')
            ->orderBy('d.exerciceBudget', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * Récupère le nombre total de kilomètres pour une année précise
     * Est utilisé pour calculer le prix d'un déplacement via le PriceCalculator
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function findTotalYearKilometers( Intervenant $intervenant, $exerciceBudget)
    {
        $query = $this->createQueryBuilder('d')
            ->select('SUM(d.kms) as kilometers')
            ->where('d.Intervenant = :intervenant')
            ->setParameter('intervenant', $intervenant)
            ->setParameter('exerciceBudget', $exerciceBudget)
            ->andWhere('d.exerciceBudget = :exerciceBudget')
            ->getQuery()
            ->getSingleScalarResult();


        return $query;
    }

    /**
     * Récupère la liste des déplacements pour l'affichage dans la section déplacements
     * @param Intervenant $intervenant
     * @return Query
     */
    public function findDeplacements( Intervenant $intervenant ): Query
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.Intervenant = :intervenant')
            ->setParameter('intervenant', $intervenant)
            ->orderBy('d.dateDeplacement', 'desc')
            ->getQuery();
    }

    /**
     *
     * @param Intervenant $intervenant
     * @return int|mixed|string
     * permet d'extraire les données lors de l'export en CSV disponible dans le controller intervenant
     */
    public function queryForCsv( Intervenant $intervenant )
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.Intervenant = :intervenant')
            ->setParameter('intervenant', $intervenant)
            ->getQuery()
            ->getResult();
    }

}
