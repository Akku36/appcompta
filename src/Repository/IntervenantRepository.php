<?php

namespace App\Repository;

use App\Custom\Search;
use App\Entity\Intervenant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Intervenant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Intervenant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Intervenant[]    findAll()
 * @method Intervenant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IntervenantRepository extends ServiceEntityRepository
{
    public function __construct( ManagerRegistry $registry )
    {
        parent::__construct($registry, Intervenant::class);
    }

    /**
     * @param Search $search
     * @return int|mixed[]|string
     */
    public function findWithSearch( Search $search )
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.nom LIKE :string')
            ->orWhere("concat(i.nom, ' ', i.prenom) LIKE :string")
            ->orWhere("concat(i.prenom, ' ', i.nom) LIKE :string")
            ->setParameter('string', "%$search->string%")
            ->getQuery()
            ->getArrayResult();

    }

    public function findTotalIntervenant()
    {
        return $this->createQueryBuilder('i')
            ->select('count(i.matricule)')
            ->getQuery()
            ->getSingleScalarResult();
    }


}
