<?php

namespace App\Repository;

use App\Entity\Intervenant;
use App\Entity\LDI;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LDI|null find($id, $lockMode = null, $lockVersion = null)
 * @method LDI|null findOneBy(array $criteria, array $orderBy = null)
 * @method LDI[]    findAll()
 * @method LDI[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LDIRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LDI::class);
    }

    /**
     * @param Intervenant $intervenant
     * @return Query
     */
    public function findLdis(Intervenant $intervenant): Query
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.matricule = :intervenant')
            ->setParameter('intervenant', $intervenant)
            ->orderBy("l.ldiNumero", 'DESC')
            ->getQuery();
        }


    // /**
    //  * @return LDI[] Returns an array of LDI objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LDI
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
