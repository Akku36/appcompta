<?php

namespace App\Controller;

use App\Custom\Search;
use App\Entity\Intervenant;
use App\Form\SearchType;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param Request $request
     * @return Response
     */
   public function index(Request $request): Response
    {
        $search = new Search();
        $intervenant = null;
        $intervenantCount = $this->getDoctrine()->getRepository(Intervenant::class)->findTotalIntervenant();
        $warning = "";
        $form = $this->createForm(SearchType::class, $search);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $intervenant = $this->getDoctrine()->getRepository(Intervenant::class)->findWithSearch($search);

            if (empty($intervenant)) {
                $warning = "Aucun intervenant ne correspond à votre recherche";
            }

        }



        return $this->render('home/index.html.twig', [
           'form'=> $form->createView(),
           'intervenants' => $intervenant,
           'total' => $intervenantCount,
           'alerte' => $warning
        ]);
    }



}
