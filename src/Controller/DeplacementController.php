<?php

namespace App\Controller;

use App\Custom\PriceCalculator;
use App\Entity\Deplacement;
use App\Entity\Intervenant;
use App\Entity\LDI;
use App\Form\DeplacementType;
use App\Repository\DeplacementRepository;
use App\Repository\SeuilRepository;
use DateTime;
use DateTimeZone;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/intervenant/{matricule}/deplacement")
 */
class DeplacementController extends AbstractController
{
    /**
     * @Route("/new", name="deplacement_new", methods={"GET","POST"})
     * @param Request $request
     * @param $matricule
     * @return Response
     * @throws Exception
     */
    public function new( Request $request, $matricule, DeplacementRepository $deplacementRepository, SeuilRepository $seuilRepository ): Response
    {
        $intervenant = $this->getDoctrine()->getRepository(Intervenant::class)->find($matricule);


        $deplacement = new Deplacement();
        $coutTrajet = new PriceCalculator();


        $form = $this->createForm(DeplacementType::class, $deplacement, [
            'matricule' => $matricule,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $regul = $form->get('isRegul')->getData();
            $km = $form->get('kms')->getData();
            $exerciceBudget = $form->get('exerciceBudget')->getData();

            $deplacement->setCreateur($this->getUser()->getPrenom() . " " .  $this->getUser()->getNom());
            $validationCompta = $form->get('validationCompta')->getData();
            if ($validationCompta == true) {
                $deplacement->setDateValidation(new DateTime('now', new DateTimeZone('Europe/Paris')));
            } else {
                $deplacement->setDateValidation(null);
            }

            //On calcul le cout uniquement si ce n'est pas un déplacement de régulation
            if ($regul === false) {
                $deplacement->setMontant($coutTrajet->calculator(
                    $km,
                    $deplacement->getVehicule()->getPuissance()->getLibellePuissance(),
                    $deplacement->getVehicule()->getTypeVehicule()->getLibelleTypeVehicule(),
                    $deplacementRepository->findTotalYearKilometers($intervenant, $exerciceBudget),
                    $seuilRepository
                ));
            } else {
                $deplacement->setMontant(0);
                $deplacement->setValidationCompta(true);
                $deplacement->setDateValidation(new DateTime('now'));
            }


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($deplacement);
            $entityManager->flush();

            return $this->redirectToRoute('intervenant_index', ['matricule' => $intervenant->getMatricule()]);
        }

        return $this->render('deplacement/new.html.twig', [
            'deplacement' => $deplacement,
            'form' => $form->createView(),
            'intervenant' => $intervenant,
            'inputReadOnly' => false,
        ]);
    }

    /**
     * @Route("/{id}", name="deplacement_show", methods={"GET"})
     * @param Deplacement $deplacement
     * @return Response
     */
    public function show( Deplacement $deplacement ): Response
    {
        return $this->render('deplacement/index.html.twig', [
            'deplacement' => $deplacement,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="deplacement_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Deplacement $deplacement
     * @param $matricule
     * @return Response
     * @throws Exception
     */
    public function edit( Request $request, Deplacement $deplacement, $matricule, DeplacementRepository $deplacementRepository, SeuilRepository $seuilRepository ): Response
    {
        $intervenant = $this->getDoctrine()->getRepository(Intervenant::class)->find($matricule);

        $form = $this->createForm(DeplacementType::class, $deplacement, [
            'matricule' => $intervenant->getMatricule()
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $regul = $form->get('isRegul')->getData();
            $km = $form->get('kms')->getData();
            $validationCompta = $form->get('validationCompta')->getData();
            $exerciceBudget = $form->get('exerciceBudget')->getData();

            if ($regul === true) {
                $deplacement->setMontant(0);
            }
            if ($validationCompta == true) {
                $deplacement->setDateValidation(new DateTime('now', new DateTimeZone('Europe/Paris')));
            } else {
                $deplacement->setDateValidation(null);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('intervenant_index', ['matricule' => $intervenant->getMatricule()]);
        }

        return $this->render('deplacement/edit.html.twig', [
            'deplacement' => $deplacement,
            'form' => $form->createView(),
            'intervenant' => $intervenant,
            'inputReadOnly' => true
        ]);
    }

    /**
     * @Route("/{id}", name="deplacement_delete", methods={"POST"})
     * @param Request $request
     * @param Deplacement $deplacement
     * @param $matricule
     * @return Response
     */
    public function delete( Request $request, Deplacement $deplacement, $matricule ): Response
    {
        $intervenant = $this->getDoctrine()->getRepository(Intervenant::class)->find($matricule);


        if ($this->isCsrfTokenValid('delete' . $deplacement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($deplacement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('intervenant_index', ['matricule' => $intervenant->getMatricule()]);
    }



}
