<?php

namespace App\Controller;

use App\Entity\Intervenant;
use App\Entity\Vehicule;
use App\Form\VehiculeType;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/intervenant/{matricule}/vehicule")
 */
class VehiculeController extends AbstractController
{
    /**
     * @Route("/new", name="vehicule_new", methods={"GET","POST"})
     * @param Request $request
     * @param $matricule
     * @param SluggerInterface $slugger
     * @param FileUploader $fileUploader
     * @return Response
     */
    public function new(Request $request, $matricule, SluggerInterface $slugger, FileUploader $fileUploader): Response
    {
        $intervenant = $this->getDoctrine()->getRepository(Intervenant::class)->find($matricule);
        $vehicule = new Vehicule();
        if (isset($intervenant)) {
            $form = $this->createForm(VehiculeType::class, $vehicule, [
                'matricule' => $intervenant->getMatricule()
            ]);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('carteGrise')->getData();
            if ($uploadedFile) {
                $newFileName = $fileUploader->upload($uploadedFile, $intervenant->getMatricule());
                $vehicule->setCarteGrise($newFileName);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($vehicule);
            $entityManager->flush();

            return $this->redirectToRoute('intervenant_index', ['matricule' => $intervenant->getMatricule()]);
        }

        return $this->render('vehicule/new.html.twig', [
            'intervenant' => $intervenant,
            'vehicule' => $vehicule,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="vehicule_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Vehicule $vehicule
     * @param $matricule
     * @param FileUploader $fileUploader
     * @return Response
     */
    public function edit(Request $request, Vehicule $vehicule, $matricule, FileUploader $fileUploader): Response
    {
        $intervenant = $this->getDoctrine()->getRepository(Intervenant::class)->find($matricule);
        $oldCG = $vehicule->getCarteGrise();

        if (isset($intervenant)) {
            // Matricule de l'intervenant rempli automatiquement (ps : le champ est caché dans la vue)
            $form = $this->createForm(VehiculeType::class, $vehicule, [
                'matricule' => $intervenant->getMatricule()
            ]);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('carteGrise')->getData();

            if ($uploadedFile !== null && !empty($vehicule->getCarteGrise())) {
                $fileUploader->removeFile($oldCG, $intervenant->getMatricule());
                $newFileName = $fileUploader->upload($uploadedFile, $intervenant->getMatricule());
                $vehicule->setCarteGrise($newFileName);
            } elseif ($uploadedFile !== null) {
                $newFileName = $fileUploader->upload($uploadedFile, $intervenant->getMatricule());
                $vehicule->setCarteGrise($newFileName);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('intervenant_index', ['matricule' => $intervenant->getMatricule()]);
        }

        return $this->render('vehicule/edit.html.twig', [
            'vehicule' => $vehicule,
            'form' => $form->createView(),
            'intervenant' => $intervenant
        ]);
    }

    /**
     * @Route("/{id}", name="vehicule_delete", methods={"POST"})
     * @param Request $request
     * @param Vehicule $vehicule
     * @param $matricule
     * @param FileUploader $fileUploader
     * @return Response
     */
    public function delete(Request $request, Vehicule $vehicule, $matricule, FileUploader $fileUploader): Response
    {
        $intervenant = $this->getDoctrine()->getRepository(Intervenant::class)->find($matricule);
        if ($this->isCsrfTokenValid('delete' . $vehicule->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            //Suppression de la carte grise
            if (!empty($vehicule->getCarteGrise())) {
                $fileUploader->removeFile($vehicule->getCarteGrise(), $intervenant->getMatricule());
            }
            $vehicule->setActiver(false);
            $vehicule->setCarteGrise(null);
            $entityManager->flush();
        }
            return $this->redirectToRoute('intervenant_index', ['matricule' => $intervenant->getMatricule()]);

    }

}
