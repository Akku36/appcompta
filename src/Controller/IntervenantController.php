<?php

namespace App\Controller;


use App\Entity\Deplacement;
use App\Entity\Intervenant;
use App\Entity\LDI;
use App\Entity\Vehicule;
use App\Form\IntervenantType;
use App\Repository\DeplacementRepository;
use App\Repository\VehiculeRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;


class IntervenantController extends AbstractController

{
    /**
     *
     * @Route("/intervenant/{matricule}", name="intervenant_index")
     * @param Request $request
     * @param Intervenant $intervenant
     * @param PaginatorInterface $paginator
     * @return Response
     */
    final public function index( Request $request, Intervenant $intervenant, PaginatorInterface $paginator ): Response
    {
        $kilometerPerYear = $this->getDoctrine()->getRepository(Deplacement::class)->findKilometersPerYear($intervenant);
        $deplacement = $paginator->paginate(
            $this->getDoctrine()->getRepository(Deplacement::class)->findDeplacements($intervenant),
            $request->query->getInt('deplacements', 1),
            10,
            array(
                'pageParameterName' => 'deplacements',
                'sortFieldParameterName' => 'sort1',
                'sortDirectionParameterName' => 'direction1',
            )
        );
        // On récupère les LDIS pour l'affichage dans la section prévue
        $intervenantLdis = $paginator->paginate(
            $this->getDoctrine()->getRepository(LDI::class)->findLdis($intervenant),
            $request->query->getInt('page', 1),
            3
        );
        if (!$intervenant) {
            return $this->redirectToRoute('home');
        }

        $vehicules = $this->getDoctrine()->getRepository(Vehicule::class)->findActivatedVehicule($intervenant);
        return $this->render('intervenant/index.html.twig', [
            'intervenant' => $intervenant,
            'deplacements' => $deplacement,
            'compteur' => $kilometerPerYear,
            'Ldis' => $intervenantLdis,
            'vehicules' => $vehicules
        ]);

    }

    /**
     * @Route("/intervenant/{matricule}/details", name="show_intervenant")
     * @param Intervenant $intervenant
     * @return Response
     */
    final public function show( Intervenant $intervenant ): Response

    {

        return $this->render('intervenant/show.html.twig', [
            'intervenant' => $intervenant,
        ]);

    }

    /**
     * L'icone de modification est masquée le fichier html intervenant/index.html.twig
     * @Route("/intervenant/{id}/edit", name="intervenant_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Intervenant $intervenant
     * @return Response
     */
    public function edit( Request $request, Intervenant $intervenant ): Response
    {
        $form = $this->createForm(IntervenantType::class, $intervenant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('intervenant_index', ['matricule' => $intervenant->getMatricule()]);
        }

        return $this->render('intervenant/edit.html.twig', [
            'intervenant' => $intervenant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/intervenant/{matricule}/csv", name="intervenant_csv")
     * @param DeplacementRepository $deplacementRepository
     * @param Intervenant $intervenant
     * @return RedirectResponse|Response
     */
    public function exportCSV( DeplacementRepository $deplacementRepository, Intervenant $intervenant )
    {
        $deplacements = $deplacementRepository->queryForCsv($intervenant);
        $headers = ["Vehicule","Puissance fiscale","Ldi numero","Kilometres", "Montant", "Date", "Exercice Budget", "Paiement","Date de validation", "Createur" ];
        $rows = array();
        foreach ($deplacements as $deplacement) {
            $data = array(
                $deplacement->getVehicule()->getImmatriculation(),
                $deplacement->getVehicule()->getPuissance(),
                $deplacement->getLdi()->getLdiNumero(),
                $deplacement->getKms(),
                $deplacement->getMontant(),
                $deplacement->getDateDeplacement()->format('d-m-Y'),
                $deplacement->getExerciceBudget(),
            );
            if ($deplacement->getDateValidation() == null && $deplacement->getValidationCompta() == false) {
                $dateNull = "Aucune";
                $validation = "Non";
                array_push($data, $dateNull, $validation);
            } else {
                $validation = "Oui";
                array_push($data, $deplacement->getDateValidation()->format('d-m-Y'), $validation);
            }
            array_push($data, $deplacement->getCreateur());

            $rows[] = implode(';', $data);

        }
        $content = implode(";", $headers);
        $content .= "\n";
        $content .= implode("\n", $rows);

        $response = new Response($content);
        $response->headers->set('Content-Encoding', 'UTF-8');
        $response->headers->set('Content-Type', 'text/csv; charset=UTF-8');
        $response->headers->set('Content-Disposition', 'attachment; filename=' . $intervenant->getNom() . "_" . $intervenant->getPrenom() . "_" . 'deplacement.csv');
        return $response;
        return $this->redirectToRoute('intervenant_index', ['matricule' => $intervenant->getMatricule()]);

    }

}
