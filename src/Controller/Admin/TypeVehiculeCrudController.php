<?php

namespace App\Controller\Admin;

use App\Entity\TypeVehicule;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class TypeVehiculeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TypeVehicule::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
