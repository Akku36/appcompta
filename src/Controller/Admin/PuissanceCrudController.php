<?php

namespace App\Controller\Admin;

use App\Entity\Puissance;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class PuissanceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Puissance::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
