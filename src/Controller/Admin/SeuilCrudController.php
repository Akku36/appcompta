<?php

namespace App\Controller\Admin;

use App\Entity\Seuil;
use Doctrine\DBAL\Types\IntegerType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class SeuilCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Seuil::class;
    }


    public function configureFields( string $pageName ): iterable
    {
        return [
            NumberField::new('seuilMin'),
            NumberField::new('seuilMax'),
            NumberField::new('prixKm'),
            AssociationField::new('puissanceFiscale'),
            AssociationField::new('typeVehicule'),
        ];
    }

}
